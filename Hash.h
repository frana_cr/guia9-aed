// Se declaran las librerias a utilizar
#include <iostream>
#include "Nodo.h" // Se define el archivo cabecera del nodo

using namespace std;

#ifndef HASH_H
#define HASH_H

// Se declara la clase Hash
class Hash{
  // Se declaran los atributos privados
  private:
  // Se declaran los atributos publicos
  public:
    Hash(); // Constructor
    void imprimirLista(Nodo **nodoAuxiliar, int *arreglo); // Esta funcion permite imprimir la lista de la funcion encadenamiento
    void colisionPruebaLineal(int *arreglo, int posicionDato, int claveDato); // Esta funcion resuelve las colisiones por prueba lineal
    void colisionPruebaCuadratica(int *arreglo, int posicionDato, int claveDato); // Esta funcion resuelve las colisiones por prueba cuadratica
    void colisionDobleDireccionHash(int *arreglo, int posicionDato, int claveDato); // Esta funcion resuelve las colisiones por doble direccion hash
    void colisionEncadenamiento(int *arreglo, int posicionDato, int claveDato); // Esta funcion resuelve las colisiones por encadenamiento
    void funcionHashPruebaLineal(int *arreglo, int claveDato);  // esta funcion recopila la informacion del modulo y agrega la prueba lineal
    void funcionHashPruebaCuadratica(int *arreglo, int claveDato); // esta funcion recopila la informacion del modulo y agrega la prueba cuadratica
    void funcionHashDobleDireccion(int *arreglo, int claveDato); // esta funcion recopila la informacion del modulo y agrega la funcion de doble direccion
    void funcionHashEncadenamiento(int *arreglo, int claveDato); // esta funcion recopila la informacion del modulo y agrega el encadenamiento



};
#endif
