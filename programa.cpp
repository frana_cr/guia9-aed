// Se define las librerias del programa
#include <iostream>
#include <string.h>

#include "Hash.h" // Se llama al archivo cabecera

using namespace std;

// Se llama a la funcion imprimir
void imprimir(int *arreglo){
  cout << "--------------------------------------------------" << endl;
  cout << " IMPRESION DATOS DEL ARREGLO" << endl;
  cout << "--------------------------------------------------" << endl;
  // Se recorre el arreglo y se imprime el arreglo
    for(int i = 0; i < 21 ; i++){
        cout << " " << " [" << arreglo[i] << "]" << endl;
    }
     cout << " " << "\r\n";
}

// Se generan las opciones del menu de opciones para el usuario
int opciones_menu(){
  int opcion;
    // Se imprimen las opciones
  cout << "\n MENU OPCIONES BUSQUEDA \n" << endl;
	cout << "OPCION 1 - INGRESAR NUMEROS" << endl;    // Esta opcion permite ingresar numero
  cout << "OPCION 2 - BUSCAR UN NUMERO" << endl;     // Esta opcion permite buscar numeros
	cout << "OPCION 0 - SALIR DEL PROGRAMA" << endl;  //OPCION QUE PERMITE SALIR DEL PROGRAMA

	// Se imprime para que el usuario ingrese su opcion y se captura el valor
	cout << "Ingrese su opcion:";
	cin >> opcion;
	return opcion;
}

// Se genera un menu de opciones para la prueba lineal
void menuOpcionesLineal(char *argv[]){
  // Se definen sus variables y los tipos
  int numero;
  int opcion;
  int tamano = 21;      // El tamano del arreglo esta determinado con 21
  int arreglo[tamano];
  // Se llama al constructor con un nuevo nombre
  Hash hash;

  opcion = 1;
  // Se genera el while con las opciones
  while(opcion != 0){
    opcion = opciones_menu();
    switch(opcion){
      // EN EL CASO 1 SE INGRESAN LOS NUMEROS
      case 1:
        cout << " Ingrese el numero:" << endl;
        cin >> numero; // Se capta el DATOS
        imprimir(arreglo); // Se imprime el arreglo
        hash.funcionHashPruebaLineal(arreglo, numero); // Se genera la funcion hash de la prueba lineal
        break;
        opciones_menu();
        // EN EN EL CASO 2 se busca un dato
      case 2:
        cout << "Ingrese el numero a buscar:" << endl;
        cin >> numero;
        // SE REALIZA LA BUSQUEDA
        for (int i=0; i< 21; i++){ // Se recorre el arreglo
          if (arreglo[i] == numero){ // Si el arreglo tiene es igual al numero
            cout << "El dato " << "[" << numero << "]"  << " esta en la posicion" << "[" << i << "]" << endl;  // Se imprime la posicion con el numero
          }
        }
      }
    }
  cout << "Saliendo del programa" << endl; // el programa luego finaliza
  exit(0);
}
// Se genera un menu de opciones para la prueba cuadratica
void menuOpcionesCuadratica(char *argv[]){
    // Se definen sus variables y los tipos
  int numero;
  int opcion;
  int tamano = 21; // El tamaño del arreglo sera 21
  int arreglo[tamano];
    // Se llama al constructor con un nuevo nombre
  Hash hash;
  opcion = 1;
  // Se genera el ciclo while con las opciones
  while(opcion != 0){
    opcion = opciones_menu();
    switch(opcion){
      // EN EL CASO 1 SE INGRESAN LOS NUMEROS
      case 1:
        cout << " Ingrese el numero:" << endl;
        cin >> numero; // Se capta el DATOS
        imprimir(arreglo);  // Se imprime el arreglo
        hash.funcionHashPruebaLineal(arreglo, numero); // Se genera la funcion hash de la prueba cuadratica
        break;
        opciones_menu();
        // EN EN EL CASO 2 se busca un dato
      case 2:
        cout << "Ingrese el numero a buscar:" << endl; // se pide el numero a buscar
        cin >> numero;
        // SE REALIZA LA BUSQUEDA
        for (int i=0; i< 21; i++){ // se recorre el arreglo y se establcen las comparativas de los datos
          if (arreglo[i] == numero){
            cout << "El dato " << "[" << numero << "]" << " esta en la posicion" << "[" << i << "]" << endl; // se imprime el numero y la posicion
          }
        }
      }
    }
    // finalmente el programa se cierra
  cout << "Saliendo del programa" << endl;
  exit(0);
}

// Se genera un menu de opciones para la prueba de doble direccion
void menuOpcionesDobleDireccion(char *argv[]){
    // Se definen sus variables y los tipos
  int numero;
  int opcion;
  int tamano = 21;
  int arreglo[tamano];
  Hash hash; // se llama al constructor y se le otorga otro nombre
  opcion = 1;
    // Se genera el ciclo while con las opciones
  while(opcion != 0){
    opcion = opciones_menu();
    switch(opcion){
      // EN EL CASO 1 SE INGRESAN LOS NUMEROS
      case 1:
        cout << " Ingrese el numero:" << endl;
        cin >> numero; // Se capta el DATOS
        imprimir(arreglo); // Se imprime el arreglo
        hash.funcionHashDobleDireccion(arreglo, numero); // Se genera la funcion hash de la prueba de doble direccion
        break;
        opciones_menu();
        // EN EN EL CASO 2 se busca un dato
      case 2:
        cout << "Ingrese el numero a buscar:" << endl; // se ingresa el numeor a buscar
        cin >> numero; // Se capta el DATOS
        // SE REALIZA LA BUSQUEDA
        for (int i=0; i< 21; i++){ // por medio del ciclo for se recorre el arreglo y se compara con el numero
          if (arreglo[i] == numero){
            cout << "El dato " << numero << " esta en la posicion" << "[" << i << "]" << endl; // se imprime el numero y la posicion
          }
        }
      }
    }
  cout << "Saliendo del programa" << endl; // finalmente se sale del programa
  exit(0);
}

// Se genera un menu de opciones para la prueba encadenamiento
void menuOpcionesEncadenamiento(char *argv[]){
    // Se definen sus variables y los tipos
  int numero;
  int opcion;
  int tamano = 21; // EL tamaño se define como 21
  int arreglo[tamano];
  Hash hash; // se llama al cosntructor y se le da otro nombre
  opcion = 1;

    // Se genera el ciclo while con las opciones
  while(opcion != 0){
    opcion = opciones_menu();
    switch(opcion){
      // EN EL CASO 1 SE INGRESAN LOS NUMEROS
      case 1:
        cout << " Ingrese el numero:" << endl;
        cin >> numero; // Se capta el DATOS
        hash.funcionHashEncadenamiento(arreglo, numero); // se llama a la funcion de encadenamiento la impresion se realiza dentro de la funcion que ve las colisiones
        break;
        opciones_menu();
        // EN EN EL CASO 2 se busca un dato
      case 2:
        cout << "Ingrese el numero a buscar:" << endl; // se ingresa el numero a buscar
        cin >> numero; // Se capta el DATOS
        // SE REALIZA LA BUSQUEDA
        for (int i=0; i< 21; i++){ // se recorre el arreglo y se compara con el numero
          if (arreglo[i] == numero){
            cout << "El dato " << numero << " esta en la posicion" << "[" << i << "]" << endl; // se imprime el numero con la posicion
          }
        }
      }
    }
  cout << "Saliendo del programa" << endl; // finalmente se sale del programa
  exit(0);
}

// Se genera el menu principal del programa donde se interactua
int main(int argc, char *argv[]){
  int arreglo[21];
  Hash hash;
  string parametro= argv[1];
  // SE COMIENZA A ENTRAR AL VISUALIZADOR DE METODOS DE BUSQUEDA
  cout << "--------------------------------------------------" << endl;
  cout << "BIENVENIDO AL VISUALIZADOR DE METODOS DE BUSQUEDA" << endl;
  cout << "--------------------------------------------------" << endl;
  // Se imprime la informacion de los parametros que ingresa el usuario
  cout << "Usted ingreso : " << argc << " parametros " << endl;
  cout << "El parametro para la resolucion de colision escogido es: " << argv[1] << endl;
  cout << "--------------------------------------------------" << endl;

// SI el usuario ingresa 2 parametros estara correcto
  if(argc == 2){
    // SI ingresa el parametro L se resuelve por prueba lineal
    if(parametro == "L"){
      cout << " RESOLUCION COLISION POR PRUEBA LINEAL" << endl;
      menuOpcionesLineal(argv);
    }
    // SI ingresa el parametro C se resuelve por prueba cuadratica
    else if(parametro == "C"){
      cout << " RESOLUCION COLISION POR PRUEBA CUADRATICA" << endl;
      menuOpcionesCuadratica(argv);
    }
    // SI ingresa el parametro L se resuelve la colision por doble direccion
    else if(parametro == "D"){
      cout << " RESOLUCION COLISION POR DOBLE DIRECCION" << endl;
      menuOpcionesCuadratica(argv);
    }
    // SI ingresa el parametro L se resuelve por encadenamiento
    else if(parametro == "E"){
      cout << " RESOLUCION COLISION POR ENCADENAMIENTO" << endl;
      menuOpcionesEncadenamiento(argv);

    }
    // EN CASO CONTRARIO QUE NO SEA NINGUNA DE ESTAS VARIABLES SE IMPRIME UN MENSAJE AL USUARIO
    else{
      cout << "Entrada no valida para resolver la colision" << endl;
      cout << "Ingrese L: Para prueba lineal , C: Para prueba cuadratica , D: Para Doble direccion , E: Para Encadenamiento" << endl;
      exit(0);
    }
  }
  // Si el valor de entrada del parametro no es valido se cierra el programa SOLO tiene que ser 2.
  else{
    cout << " El valor de entrada del parametro es invalido" << endl;
    exit(0);
  }

  return 0;
}
