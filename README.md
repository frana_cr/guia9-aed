# Guia9-AED - METODOS DE BUSQUEDA -

# Comenzando --- EXPLICACION PROBLEMATICA----
La problematica de la novena guia requiere que se almacene una cantidad n de elementos de un arreglo, se tiene que permitir a su vez el ingreso y busqueda de la informacion. Tambien se quiere una funcion Hash la cual tiene que distribuir los datos de registro del arreglo para poder realizar una busqueda si hay datos sueltos se tienen que aplicar los metodos de reasignacion de prueba lineal, prueba cuadratica y tambien de doble direccion hash y finalmente tambien se debe ocupar encadenamiento. Es muy importante que cuando el usuario ingrese por terminar pueda ingresar parametros de entrada con los metodos de resolucion de colisiones. Cada vez que el usuario ingrese algun valor debe imprimir el contenido del arreglo y las listas enlazadas corresponientes, tambien si existe una colision debe indicar donde y cual fue el desplazamiento. Cada vez que se realice una busqueda debe indicar en la posicion donde se encuentra y si ocurrio una colision indicar en donde y cual fue el desplazamiento final. 

# ---- COMO SE DESARROLLO ----
Para poder resolver la problematica propuesta en la guia lo primero que se realizo fue la comprension del contenido de los metodos de busqueda. Luego se procedio a diseñar el modelo de la resolucion de las problematicas definiendo las funciones que pueden ser las mas eficientes y capaces de buscar una clara solucion al problema, se trato de buscar una forma de mas eficiente para poder mostrar los datos y que los metodos de busqueda funcionen al igual se fue indentificando como mostrara las colisiones de las listas enlazadas del arreglo. Los archivos principales son detallados a continuacion con su funcionamiento y caracteristicas propias.

- _Makefile:_  Permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. Es un requerimiento imprescindible dentro de los programas para poder ver su funcionalidad.

- _Nodo.h:_ Este archivo contiene la estructura basica del nodo la cual permite que pueda ser llamado en los distintos archivos de esta manera se puede desarrollar de manera global y contiene todas las estructuras necesarias las cuales pueden ser los nodos siguientes del arreglo, tambien se necesita el valor para poder ir recorriendo el arreglo en forma de lista, el valor esta definido como una variable entera, el nodo a su vez es fundamental para poder desarrollar la funcion de encadenamiento, permitiendo solucionar las colisiones presentes en la lista. En este caso es necesaria para que el metodo de resolucion de colisioones por encadenamiento pueda ser completado. 

- _ Hash.h:_ Este archivo contiene la informacion necesaria para ser la cabecera del archivo Hash.cpp contiene informacion sobre los atributos privados como publicos y funciones de las clases. Contiene la estructura basica para que el código pueda funcionar y se genere la clase hash la cual permite que pueda ser llamado en los distintos archivos de esta manera se puede desarrollar de manera global. Las clases principales del programa tienen que ser la resolucion de los metodos de colisiones que pueden surgir luego de que se genere la funcion Hash, se implemento una clase por cada metodo y las tecnicas para su construccion y la posterior realizacion son basadas en el libro base del curso.

- _Hash.cpp:_ En este archivo se llama al archivo cabecera el cual contiene las indicaciones de como deben funcionar las funciones, se llama al constructor de las funciones que en este caso de la clase hash del arreglo . Lo que se realiza en este archivo es poder llevar a cabo las validaciones de los metodos de solucion de colisiones tambien dentro de cada una de las funciones esta la funcion hash que es primordial para poder redistribuir el arreglo, en el caso del metodo de encadenamiento tiene que ser una lista. Las funciones principales del programa son: 

    - imprimirLista: Esta funcion es la encargada de poder imprimir las listas cuando se realiza el metodo de encadenamiento de esta manera se puede comprender como esta funcionando la colision y tambien luego de que se termine de tratar de ver la colision se puede observar como la lista se va actualizando a medida que se agregan numeros de ingreso. Para poder ver como los numeros son impresos es necesario tener que recorrer el nodo de la estructura y a su vez hay que recorrer un ciclo for. 

    - colisionPruebaLineal: Este metodo de solucion de colisiones consiste en que luego de poder detectar donde se encuentra la colision recorre el arreglo de una manera secuencial hasta el elemento siguiente que se encuentre vacio, la estructura del arreglo se trata como una estrucutra de caracter circular desde el ultimo numero hasta el primero. Durante la construccion de esta funcion al comenzar se tiene que declarar la funcion Hash la cual permite la redistribucion en este caso se implemento el metodo Hash por modulo. Se van imprimiendo comentarios al usuario para que conozca donde esta la informacion del dato en el arreglo y que ocurre cuando esta no esta.

    - colisionPruebaCuadratica: El metodo de la prueba cuadratica es muy similar al metodo de la prueba lineal la unica diferencia es que las direcciones son alternativas y no siguen un orden establecido o cardinal, tambien esta variacion permite que los datos sean mejor redistribuidos y se puedan identificar de mejor manera las claves que colisionan. Para poder identificar las colisiones y como se asignan los valores en el arreglo es necesario el uso de la funcion hash por modulo. Cada vez que se encuentren las condiciones correctas entre el arreglo se imprime donde esta la posicion de la informacion y a su vez si esta no esta en el arreglo. 

    - colisionDobleDireccionHash: Este metodo consiste en detecatar una colision cuando se genera otra direccion se aplica la funcion hash tambien para poder identificar las posiciones y si es que esta presente en el arreglo el dato, este proceso finaliza cuando se encuentra el elemento o se encuentra vacio el arreglo. La funcion hash es la misma que se utiliza para realizar la busqueda la cual observa por el modulo del valor y se le suma el valor 1.

    - colisionEncadenamiento: Finalmente se lleva a cabo la solucion de las colisiones por medio del metodo de encadenamiento, para esto es necesario tener la estructura del nodo es por esto que se crea un archivo con la estructura para poder realizar una busqueda y crear un nodo, por medio de este nodo se puede llevar a cabo la busqueda del numero si es que esta en una posicion especifica, a su vez tambien se imprime cual es la lista de todos los valores que han sido agregados por el usuario para que pueda conocer los valores de la lista y a su vez si la informacion esta en la posicion esto se logra mediante la funcion imprimirLista(). Este metodo en simples palabras es capaz de generar un puntero, la lista se generara y almacenara los valores que colsiionan. La desventaja de este metodo es que si las listas crecen de manera rapida no se podra acceder al metodo de la tabla Hash.
    

    - funcionhashPruebaLineal: Esta funcion es la encargada de hacer la funcion hash y recopilar la informacon del modulo para asignar una posicion a esta, dentro de esta funcion se recorre el arreglo para poder ver donde existe una colision y si esto pasa imprime la posicion y la clave, en este caso esta funcion realiza y determina la posicion de la prueba lineal agregando el dato

    - funcionHashPruebaCuadratica: Esta funcion es la encargada de hacer la funcion hash y recopilar la informacon del modulo para asignar una posicion a esta, dentro de esta funcion se recorre el arreglo para poder ver donde existe una colision y si esto pasa imprime la posicion y la clave, en este caso esta funcion realiza y determina la posicion de la prueba cuadratica agregando el dato

    - funcionhashDobleDireccion: Esta funcion es la encargada de hacer la funcion hash y recopilar la informacon del modulo para asignar una posicion a esta, dentro de esta funcion se recorre el arreglo para poder ver donde existe una colision y si esto pasa imprime la posicion y la clave, en este caso esta funcion realiza y determina la posicion de la prueba doble direccion hash agregando el dato

    - funcionhashEncadenamiento: Esta funcion es la encargada de hacer la funcion hash y recopilar la informacon del modulo para asignar una posicion a esta, dentro de esta funcion se recorre el arreglo para poder ver donde existe una colision y si esto pasa imprime la posicion y la clave, en este caso esta funcion realiza y determina la posicion de donde se desarrollo el encadenamiento agregando el dato


_Nota:_ Se implemento la funcion hash por medio del modudlo o division en donde se toma el residuo de la division del numero clave con las componentes del arreglo, tambien finalmente se le debe sumar 1 para que el valor este dentro de un intervalo positivo y mayor o igual a 1. N tiene que ser primo por eso el tamaño del arreglo del programa es 21, en caso de que no hubiese sido un numero primo se considera al numero mas cercano a este. 

- _Programa.cpp:_ En este archivo se llevan a cabo la funcionalidad del programa la cual puede ser probada y ademas se lleva a cabo la interaccion con el usuario. Se incluyen las librerías a implementar y también se llama al archivo que contiene las funciones para que se lleve a cabo el programa(cabecera). El usuario comienza el programa y debe ejecutar el comando _./programa parametro_ para poder llevar a cabo la solucion de las posibles colisiones futuras que se generen para esto dependiendo de la opcion con el parametro que ingrese el usuario se resolvera la colision, los parametros son los siguientes:
    - Parametro "L" : por medio de este parametro las colisiones se resolveran por medio de la prueba lineal.
    - Parametro "C" : por medio de este parametro las colisiones se resuleven por medio de la prueba cuadratica.
    - Parametro "D" : por medio de este parametro las colisiones se resuleven por medio de Doble direccion Hash.
    - Parametro "E" : por medio de este parametro las colisiones se resuleven por medio del metodo de Encadenamiento. 
Si el usuario llega a ingresar un parametro distinto en cuanto a letras y cantidad se enviara un mensaje de advertencia para que tenga cuidado la proxima vez. 

Luego de ingresar el parametro, el programa define de manera fija que el tamaño del arreglo del programa sera de 21, numero definido por la persona que elaboro el programa se realizo de esta manera debido a que se genero optimamente y previniendo que el usuario ingresara errores en la digitacion de los valores, posteriormente se imprime el metodo por el cual se resolvera la colision y se imprime el menu de las opciones para el usuario, las cuales son 3, poder ingresar mas numeros mostrando el arreglo con los valores y posteriormente mostrando las colisiones, la segunda opcion es buscar un dato donde debe entregar que dato es el buscado y la posicion en donde este se encuentra tambien se tiene que mostrar si es que existe alguna colision, finalmente la ultima opcion es cerrar el programa y finalizar con las opciones.
Ademas se realizo un menu especial para cada una de las funicones de colisiones, de esta manera, se puede ver como la funcion hash de encadenamiento participa en cada una de ellas y se pueden ver la colisiones y ademas donde las claves estan localizadas ahora, cada menu tiene el nombre especifico de la prueba a cual representa,se hizo de esta manera, debido que asi se ahorraban los problemas de realizar una funicon especifica la cual no se podia tratar, a su vez permite buscar los datos y tambien ingresar otros valores, esto se realizo con los cuatro metodos de resolucion de colisiones

Se pudo resolver la problematica planteada por el profesor por medio del libro guia y por medio de busqueda de informacion en internet, se implementaron los fragmentos de codigo entregados en la presentacion del docente del modulo, tambien a su vez se realizo optimamente el funcionamiento del menu, de la impresion de los datos, la validacion de los parametros que ingresa el usuario, la funcion hash se pudo implementar y el metodo de las colisiones. Una observacion es que el programa logra cumplir la problematica pero no en su totalidad debido a que el ingreso de los valores al arreglo no se pudo solucionar pero si se genera lo que quiere decir que puede ser un vector que este mal orientado. La busqueda de los datos es funcional y dentro de los datos que se muestran si se pueden buscar y arrojan cual es el dato y la posicion. Tambien se pudo cumplir con la validacion de los parametros, el uso de clases y con la solucion de las colisiones que se pueden presentar, el usuario puede ver donde estan las colisiones y otras validaciones que se crearon en el programa por lo tanto se pudo cumplir con la mayoria de los puntos, solo falto ver en mas detalle que los datos se agreguen correctamente al arreglo, pero si se pueden llevar a cabo las otras tareas de una manera estable y el programa no se cae a no ser que se ingrese un parametro incorrecto o una opcion no deseada. La recepcion e ingreso de los datos es correcta la impresion es la que falla debido a que almacena otros datos incorrectos pero estos se pueden buscar y los ingresados tambien . 

# Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)

# Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:

- _lsb_release -a (versión de Ubuntu)_

En donde: Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando. Para descargar un editor de texto como vim se puede descargar de la siguiente manera:

- _sudo apt install vim _

En donde: Por medio de este editor de texto se puede construir el codigo. En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.
Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga, de esta manera se pudo descargar el editor que se ocupa para la realizacion de esta guia el cual es atom, se eligio a la vez este editor debido a que permite una interfaz mejorada con respecto a la compilacion y a su vez permite que se revisen y compilen los archivos de la terminal disminuyendo posibles errores.

# Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa
Para poder ejecutar el programa se tiene que hacer las siguientes condiciones:
Se debe ingresar a la carpeta donde se esta trabajando con los archivos del programa y esta debe contener el archivo Makefile, si este no ha sido probado con anterioridad luego se agrega el comando:

- _make_

De esta manera se esta compilando el trabajo, si este no presenta comentarios significa que la prueba finalizo con exito.
Por otra parte si se esta probando con el comando G++ se tiene que hacer el siguiente comando por terminal:

- _g++ programa.cpp ordenamiento.cpp_

Si la carpeta con archivos presenta otras extensiones diferentes a .cpp, .h o al archivo makefile se debe ejecutar el siguiente comando:

- _make clean_

Posteriormete se ejecuta en la terminal


- _make._

Luego se lleva a cabo el programa ejecutando el siguiente comando por terminal:
- _./programa parametro_

De esta manera se podra solucionar la colision del programa como el usuario prefiera los caracteres validos para una correcta ejecucion del programa son: 
    - "L": Para prueba lineal 
    - "C": Para prueba cuadratica
    - "D": Para doble direccion hash
    - "E": para encadenamiento

Si se ingresa un valor distinto el usuario tendra que ingresar la correcta debido a que se impremen una serie de mensajes y validaciones. 

## INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:

- _make_

De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.

- _sudo apt install make_

De esta manera se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal.
Si ya se ha ejecutado con anterioridad un make, se tiene que escribir un make clean para borrar los archivos probados con anterioridad. Luego de ejecutar en la terminal el comando:

- _make_

Debe mostrar los nombres de los archivos y el orden de estos con g++ y el nombre del archivo, aqui se puede visualizar si el programa no tiene problematicas.
Si hay una problematica sale un mensaje y no salen todos los archivos con g++. Si se desarrollo completamente luego se puede ingresar el siguiente comando:

- _./ con el nombre del programa el cual contiene el main._

De esta manera se observa el funcionamiento del programa y de los demas archivos.

# Construido con

- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.

# Versiones
## Versiones de herramientas:

- Ubuntu 20.04 LTS
- Atom 1.52.0
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia9-aed

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud

- A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=731119

- A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible:
https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&ved=2ahUKEwjpmb2yhNHtAhVhGbkGHTEfCyIQtwIwAHoECAMQAg&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D_On-0W0-6vQ&usg=AOvVaw2dzDjiERIpTkj3ZiIJB7vW

https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&ved=2ahUKEwjpmb2yhNHtAhVhGbkGHTEfCyIQtwIwA3oECAIQAg&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DrOso4Xa0bs4&usg=AOvVaw2sfEL5HjPDh4vtSIeLNmOs

https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&ved=2ahUKEwjvuuS8hNHtAhUZF7kGHUE4CDYQtwIwAHoECAAQAg&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DyUKGvDxleKI&usg=AOvVaw2tOW-3g_KUR6YJ-r3UCo6T

https://www.youtube.com/watch?v=WnLdu8OHA3Q

