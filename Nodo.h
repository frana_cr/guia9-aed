// Se declaran las librerias
#include <iostream>


// Se define la estructura del nodo
typedef struct _Nodo {
  // Se define el dato entero
  int informacion_dato;
  // Se genera la estructura del nodo siguiente
  struct _Nodo *siguiente;

// Se define el nombre del nodo
} Nodo;
