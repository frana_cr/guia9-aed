// Se declaran las librerias a utilizar
#include <iostream>
#include <string.h>

#include "Hash.h" // Se define el archivo cabecera del nodo

using namespace std;

// Se llama al cosntructor de la clase
Hash::Hash(){
}

// se genera la funcion que imprime la lista con el nodo auxiliar y el arreglo
void Hash::imprimirLista(Nodo **nodoAuxiliar, int *arreglo){
  int i;
  // se recorre el arreglo
  for(i = 0; i < 21; i++){
   nodoAuxiliar[i]->informacion_dato = arreglo[i]; // el nodo auxiliar contrendra la informacion y se igualara al arreglo
   nodoAuxiliar[i]->siguiente = NULL; // el nodo siguiente sera vacio
  while(nodoAuxiliar[i] != NULL){ // si el nodo esta vacio
    if(nodoAuxiliar[i]-> informacion_dato != 0){ // si el nodo auxiliar contiene la informacion distinta de 0
		  cout << "[" << nodoAuxiliar[i]-> informacion_dato << "] - "; // se imprime el nodo y la informacion del dato
    }
		nodoAuxiliar[i] = nodoAuxiliar[i]-> siguiente; // el nodo auxiliar sera igual a este en la posicion siguiente
  }
}
}

// CODIGO DE PRUEBA LINEAL DEL LIBRO se lleva a cabo la colision de la prueba lineal
void Hash::colisionPruebaLineal(int *arreglo, int posicionDato, int claveDato){
  int nuevaPosicionDato; // nueva posicion
  // ESTA ES LA FUNCION HASH QUE RECOMBINA EL ARREGLO
  nuevaPosicionDato = ((posicionDato + 1)%21 - 1) + 1;
  // se establec esta comparacion para ver si el dato esta en el arreglo
  if((arreglo[posicionDato]!= 0) && (arreglo[posicionDato] == claveDato)){
    // se imprime si las condiciones se cumplen
    cout << " La informacion esta en la posicion " << "[" << posicionDato << "]" << endl;
  }
  // de modo contrario a la posicion nueva se le suma 1 con la posicion antigua
  else{
    nuevaPosicionDato = posicionDato + 1;
    // se establece la compartiva para ver si el arreglo esta vacio y si cumple con el tamano
    while((nuevaPosicionDato <= 21) && (arreglo[nuevaPosicionDato] != 0) && (arreglo[nuevaPosicionDato] != claveDato) && (nuevaPosicionDato != posicionDato)){
      // posicion nueva se le suma 1 con la posicion antigua
      nuevaPosicionDato = nuevaPosicionDato + 1;
      // si la posicion del dato es igual a 22 un numero mas del arreglo
      if(nuevaPosicionDato == 22){
        nuevaPosicionDato = 1; // la posicion sera igual a 1
      }
    arreglo[nuevaPosicionDato] = claveDato;
    }
    // si el arreglo esta en laposicion 0 y la sposiciones son iguales
    if((arreglo[nuevaPosicionDato] == 0) && (nuevaPosicionDato == posicionDato)){
      cout << " La informacion no esta en el arreglo" << endl; // LA INFO NO ESTA EN EL ARREGLO
    }
    // SI EL ARREGLO TIENE UNA POSICION DISTINTA A VACIO
    else if (arreglo[nuevaPosicionDato] != 0){
      arreglo[nuevaPosicionDato] = claveDato; // ESTARA LA CLAVE EN EL ARREGLO Y SE IMPRIME
      cout << " La clave" << "[" << claveDato << "]" << "ahora esta en la posicion " << "[" << nuevaPosicionDato << "]" << endl;
    }
  }
}

// CODIGO DE PRUEBA CUADRATICA DEL LIBRO
void Hash::colisionPruebaCuadratica(int *arreglo, int posicionDato, int claveDato){
  int nuevaPosicionDato;
  int i; // i es de tipo entero para ver las posiciones y llevar a cabo la solucion 
  // SE LLAMA A LA FUNCION HASH
  nuevaPosicionDato = ((posicionDato + 1)%21 - 1) + 1;
  // se establece la comparativa de que el arreglo en la posicion no es vacio y es igual a la clave
  if((arreglo[posicionDato] != 0) && (arreglo[posicionDato] == claveDato)){
        // se imprime si las condiciones se cumplen
    cout << "La informacion esta en la posicion" << "[" << posicionDato << "]" << endl;
  }
  // de modo contrario ocurrira lo siguiente
  else{
    // el valor de 1 sera igual a 1
    i = 1;
    // la nueva posicion sera igual a la posicion por la multiplicacion de i
    nuevaPosicionDato = (posicionDato + (i*i));
    // cuando no se cumple que el arreglo en l anueva posicion es diferente de 0 y el arreglo en la nueva posicion es distinto de 0
    while((arreglo[nuevaPosicionDato] != 0) && (arreglo[nuevaPosicionDato] != claveDato)){
      // el valor de i se le sumara 1 por lo tanto aumentara
      i = i + 1;
      // la nueva posicion es igual a la nueva posicion mas la suma de la multiplicacion de 1
      nuevaPosicionDato = (posicionDato +(i * i));
      // si la nueva posicion del dato es diferente al tamano
      if (nuevaPosicionDato > 21){
        // el valor de i es igual a 0
        i = 0;
        // la nueva posicion sera igual a 1
        nuevaPosicionDato = 1;
        // la posicion antigua sera igual a 1
        posicionDato = 1;
      }
    }
    // si el arreglo en la nueva posicion es 0
    if (arreglo[nuevaPosicionDato] == 0){
      // se imprime que el dato no esta en el arreglo
      cout << " La informacion no esta en el arreglo " << endl;
    }
    else{
      // de modo contrario si alguna de estas dos condiciones no se cumple se imprime la clave y la posicion
      cout << "La clave " << "[" << claveDato << "]" << " Esta en la posicion" <<  "[" << nuevaPosicionDato << "]" << endl;
    }
  }
}

// SE VE LA COLISION EN DOBLE DIRECCION COMO EL LIBRO
void Hash::colisionDobleDireccionHash(int *arreglo, int posicionDato, int claveDato){
  int nuevaPosicionDato;
    // SE LLAMA A LA FUNCION HASH
  nuevaPosicionDato = ((posicionDato + 1)%21 - 1) + 1;
    // se establece la comparativa de que el arreglo en la posicion no es vacio y es igual a la clave
  if((arreglo[posicionDato] != 0) && (arreglo[posicionDato] == claveDato)){
        // se imprime si las condiciones se cumplen
    cout << " La informacion esta en la posicion" << "[" << posicionDato << "]"<< endl;
  }
  // de modo contrario se busca por medio del ciclo hash
  else{
        // A la nueva posicion se generara la funcion hash
    nuevaPosicionDato = ((posicionDato + 1)%21 - 1) + 1;
        // se establece la compartiva para ver si el arreglo esta vacio y si cumple con el tamano
    while((nuevaPosicionDato <= 21) && (arreglo[nuevaPosicionDato] != -1) && (arreglo[nuevaPosicionDato] != claveDato) && (nuevaPosicionDato != posicionDato)){
      // A la nueva posicion se generara la funcion hash
    nuevaPosicionDato = ((posicionDato + 1)%21 - 1) + 1;
    }
    // el arreglo en la nueva posicion sera igual a la clave del dato
    arreglo[nuevaPosicionDato] = claveDato;
    // si el arreglo en la nueva posicion es vacio y la nueva posicion es diferente a la clave
    if ((arreglo[nuevaPosicionDato] == 0) || arreglo[nuevaPosicionDato] != claveDato){
      // se imprime que el dato no esta en el arreglo
      cout << " La informacion no esta en el arreglo" << endl;
    }
    else{
      // de modo contrario si alguna de estas dos condiciones no se cumple se imprime la clave y la posicion
      cout << " La clave" << "[" << claveDato << "]" << " Esta en la posicion" << "[" << nuevaPosicionDato << "]" << endl;
   }
 }
}

// SE VE LA COLISION COMO ENCADENAMIENTO
void Hash::colisionEncadenamiento(int *arreglo, int posicionDato, int claveDato){
  //se definen los nodos para ocuparlos
  Nodo *temporal = NULL;
  Nodo *arregloAux[21];
  // se recorre el arreglo para añadir los nodos y las direcciones que tendran
  for(int i = 0; i < 21; i++){
    arregloAux[i] = new Nodo();
    arregloAux[i]->siguiente = NULL; // siguiente sera VACIO
    arregloAux[i]->informacion_dato = -1; // la informacion del dato sera igual a -1
  }
  // se hacen las ocmparaicones para pode ver si estan en la posicion los datos
  if ((arreglo[posicionDato] != -1) && (arreglo[posicionDato] == claveDato)){
    cout << " La informacion esta en la posicion" << "[" << posicionDato << "]" << endl;
  }
  else{
    // el nodo temporal sera igual al nodo auxiliar con la posicion del dato en la siguiente
    temporal = arregloAux[posicionDato]->siguiente;
    //cuando el temporal es distitno de vacio y el nodo temporal no tiene a la clave
    while((temporal != NULL) && (temporal->informacion_dato != claveDato)){
      // el nodo temporal sera igual al mismo en la posicion siguiente
      temporal = temporal->siguiente;
    }
    // si el temporal es vacio
    if(temporal == NULL){
      // la informacion no esta en la lista y se imprime cual es la lista
      cout << " La informacion no esta en la lista" << endl;
      cout << "La lista es la siguiente:" << endl;
      imprimirLista(arregloAux, arreglo); // llama a la funcion que imprime
    }
    else{
      // se llama a la clave y la posicion donde esta
      cout << "La clave" << "[" << claveDato << "]" << "Esta en la posicion" << "[" << posicionDato << "]"  << endl;
      cout << "La lista es la siguiente:" << endl;
      imprimirLista(arregloAux, arreglo); // llama a la funcion que imprime
    }
  }
}

// ESTA FUNCION COMPLETA EL ARREGLO DE LA FUNCION PRUEBA LINEAL
void Hash::funcionHashPruebaLineal(int *arreglo, int claveDato){ // Determinara la posición donde se agregara el dato
    int posicionDato;
      // SE LLAMA A LA FUNCION HASH POR MODULO PARA ASIGNAR LAS POSICIONES
    posicionDato = (claveDato%(21 - 1)) + 1;
        // SI HAY UNA COLISION
    if(arreglo[posicionDato] != -1){
        cout << "Colision en posicion " <<"[" << posicionDato << "]"  << endl;
          // SE LLAMA AL METODO QUE RESUELVE LA COLISION
        colisionPruebaLineal(arreglo, posicionDato, claveDato);
    }

    else{
            // DE MODO CONTRARIO SE IMPRIME LA POSICION IGUAL A LA CLAVE
        arreglo[posicionDato] = claveDato;
    }
}

// ESTA FUNCION COMPLETA EL ARREGLO DE LA FUNCION PRUEBA CUADRATICA
void Hash::funcionHashPruebaCuadratica(int *arreglo, int claveDato){ // Determinara la posición donde se agregara el dato
    int posicionDato;
        // SE LLAMA A LA FUNCION HASH POR MODULO PARA ASIGNAR LAS POSICIONES
    posicionDato = (claveDato%(21 - 1)) + 1;
        // SI HAY UNA COLISION
    if(arreglo[posicionDato] != -1){  // SE IMPRIME DONDE FUE LA COLISION
        cout << "Colision en la  posicion " << "[" << posicionDato << "]" << endl;
            // SE LLAMA AL METODO QUE RESUELVE LA COLISION
        colisionPruebaCuadratica(arreglo, posicionDato, claveDato);
    }

    else{
        // DE MODO CONTRARIO SE IMPRIME LA POSICION IGUAL A LA CLAVE
        arreglo[posicionDato] = claveDato;
    }
}

// ESTA FUNCION COMPLETA EL ARREGLO DE DOBLE DIRECCION
void Hash::funcionHashDobleDireccion(int *arreglo, int claveDato){ // Determinara la posición donde se agregara el dato
    int posicionDato;
    // SE LLAMA A LA FUNCION HASH POR MODULO PARA ASIGNAR LAS POSICIONES
    posicionDato = (claveDato%(21 - 1)) + 1;
      // SI HAY UNA COLISION
    if(arreglo[posicionDato] != -1){  // SE IMPRIME DONDE FUE LA COLISION
        cout << "Colision en posicion " << "[" << posicionDato << "]" << endl;
          // SE LLAMA AL METODO QUE RESUELVE LA COLISION
        colisionDobleDireccionHash(arreglo, posicionDato, claveDato);
    }
    else{
      // DE MODO CONTRARIO SE IMPRIME LA POSICION IGUAL A LA CLAVE
        arreglo[posicionDato] = claveDato;
    }
}

// ESTA FUNCION COMPLETA EL ARREGLO DE ENCADENAMIENTO
void Hash::funcionHashEncadenamiento(int *arreglo, int claveDato){ // Determinara la posición donde se agregara el dato
    int posicionDato;
    // SE LLAMA A LA FUNCION HASH POR MODULO PARA ASIGNAR LAS POSICIONES
    posicionDato = (claveDato%(21 - 1)) + 1;
    // SI HAY UNA COLISION
    if(arreglo[posicionDato] != -1){ // SE IMPRIME DONDE FUE LA COLISION
        cout << "Colision en la  posicion " << "[" << posicionDato << "]" << endl;
        // SE LLAMA AL METODO QUE RESUELVE LA COLISION
        colisionEncadenamiento(arreglo, posicionDato, claveDato);
    }
    else{
      // DE MODO CONTRARIO SE IMPRIME LA POSICION IGUAL A LA CLAVE
        arreglo[posicionDato] = claveDato;

    }
}
